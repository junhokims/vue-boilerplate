/**
 * @Project vue-boilerplate
 * @author  junho.kim
 *
 * Created by Junho Kim (junho.kim@loplat.com) on 2017. 8. 22..
 */

import Vue from 'vue';
import VueRouter from 'vue-router';

import routes from '@/routes';
import components from '@/components';

import App from '@/App.vue';


Vue.use(VueRouter);


Object.keys(components).forEach((key) => {
    Vue.component(key, components[key]);
});

const router = new VueRouter({ routes });


new Vue({
    router,
    template: '<App />',
    components: { App }
}).$mount('#app');
