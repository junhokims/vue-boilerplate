/**
 * @Project vue-boilerplate
 * @author  junho.kim
 *
 * Created by Junho Kim (junho.kim@loplat.com) on 2017. 8. 22..
 */

import config from '@/config';


export default [
    {
        path: '/',
        name: 'Index',
        component: require('@/views/Index.vue')
    }
];
