/**
 * @Project beaver
 * @author  h4wldev
 *
 * Created by h4wldev on 2017. 07. 27..
 */

import path from 'path';

import webpack from 'webpack';
import HtmlPlugin from 'html-webpack-plugin';
import RobotstxtPlugin from 'robotstxt-webpack-plugin';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import CleanWebpackPlugin from 'clean-webpack-plugin';
import AutoInjectVersionPlugin from 'webpack-auto-inject-version';
import OptimizeCssAssetsPlugin from 'optimize-css-assets-webpack-plugin';


export default {
    entry: './app.js',
    output: {
        filename: 'bundle.[hash].min.js',
        path: path.resolve(__dirname, './build')
    },
    resolve: {
        alias: {
            'vue$': 'vue/dist/vue.esm.js',
            '@': path.resolve(__dirname, './')
        }
    },
    plugins: [
        new webpack.optimize.UglifyJsPlugin({
            compress: { warnings: false },
            output: { comments: false }
        }),
        new ExtractTextPlugin('bundle.[hash].min.css'),
        new OptimizeCssAssetsPlugin({
            assetNameRegExp: /bundle\.[0-9a-zA-Z]+.min.css$/g,
            cssProcessor: require('cssnano'),
            np: { discardComments: {removeAll: true } },
            canPrint: true
        }),
        new HtmlPlugin({
            template: 'views/Layout.html',
            favicon: 'static/images/favicon.png'
        }),
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery",
            "window.jQuery": "jquery"
        }),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV || 'development'),
            'VERSION': JSON.stringify(require("./package.json").version)
        }),
        new CleanWebpackPlugin(['build']),
        new RobotstxtPlugin({
            policy: [
                {
                    userAgent: '*',
                    disallow: '/'
                }
            ]
        }),
        new AutoInjectVersionPlugin({
            components: {
                AutoIncreaseVersion: true
            }
        })
    ],
    module: {
        rules: [
            {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    use: 'css-loader',
                    fallback: 'style-loader'
                })
            },
            {
                test: /\.s[a|c]ss$/,
                use: ExtractTextPlugin.extract({
                    use: ['css-loader', 'sass-loader'],
                    fallback: 'style-loader'
                })
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader'
            },
            {
                test: /\.(png|jpe?g|gif|svg|ttf|woff2?|eot)$/,
                loader: 'file-loader',
                options: { name: '[name].[ext]?[hash]' }
            }
        ]
    }
}
